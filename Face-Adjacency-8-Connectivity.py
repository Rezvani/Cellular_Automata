import Rhino.Geometry as rg
import Grasshopper as gh
import scriptcontext as sc

def eightconnectivity(self):
    DT=[]
    FtF=[]
    n=self.Faces.Count
    for i in range(0,n):
        VerList=self.TopologyVertices.IndicesFromFace(i)
        DT.Add(VerList)
    for q in range(0,n):
        FtF.Add([])
        testA=DT[q][0]
        testC=DT[q][2]
        testB=DT[q][1]
        testD=DT[q][3]
        for f in range(0,n):
            if f!=q:
                if (testA in DT[f]) or (testB in DT[f])or (testC in DT[f]) or (testD in DT[f]):
                    FtF[q].Add(f)
    return FtF


FF=eightconnectivity(M)


