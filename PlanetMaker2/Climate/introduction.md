# Introduction
This is the folder for the Climate group for AR0078 Future Models course. 
## Contributors: 
Alex Hewitson\
Bowen Chen\
Yanjie Liu

## Contents

In the Data Merging folder you will find:
  * Python codes used to process data, as well as the raw data and clean data

In the Python Code of Data folder you will find:
  * Python codes used to process data only

In the Houdini folder you will find:
  * a Houdini model used to merge the databases for use in simulation
![](Images/data-merging.png)
  * a Houdini model of the simulation
![](Images/simulation.png)
  * various pieces of code used within the simulation

In the Images folder you will find:
  * the images you see above only