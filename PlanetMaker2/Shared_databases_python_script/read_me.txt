------
THIS FOLDER IS FOR SHARED DATASETS AND THEIR PYTHON SCRIPTS
------

this is a folder to put in all shared datasets (processed) and the python 
scripts used for that. Also throw in the country lists, the initial lists 
of value_counts and hitprim numbers. (is useful to keep)

Things that need to be in here:
- land use
- countries list
- python script to convert contries to triangles
- population
- gdp 

N.B. Also please make a text file to keep track of our sources. 