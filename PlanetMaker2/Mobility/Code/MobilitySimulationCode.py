
#_____________________________________________#
##########    MOBILITY GROUP CODE    ##########
#_____________________________________________#


node = hou.pwd()
geo = node.geometry()
groups = geo.primGroups()

import os
import networkx as nx
import math
import random as rd

prims = []
neighbours = []
graph = nx.Graph()
primNums = []
setWeight = []
centrality = []

#### getting all the primitives (houdini points with all the data stored in them with HEX-Values)

for prim in geo.prims():
    prims.append(prim)

#### getting the needed prim attributes  (extracting the needed data from the points)
    
for prim in prims:
    neighbours.append(prim.intListAttribValue('primnbrs'))
    setWeight.append(prim.attribValue('travelFactor'))
    centrality.append(prim.attribValue('Centrality'))
   


#### checking ammount of primitives (checking if the ammount of primitives corresponds with the needed points)

i = 0
while i < len(prims):
    primNums.append(i)
    i+=1
    
    
#### setting a geometric nx-graph from nodeIndex, neighbours and travelweight (A needed graph to be able to calculate with pathfinder algorithms)

for prim in prims:
    if prim.attribValue('landuse_id') > 0 and prim.attribValue('altitude') < 1000:
        index = prim.intrinsicValue('indexorder')
        nbrs = prim.intListAttribValue('primnbrs')
        travelFactor = prim.attribValue('travelFactor')
        for neigh in nbrs:
            graph.add_edge(index, neigh, weight=travelFactor)
            if  index != len(prims):
                continue

        
                
#### getting all houdini primgroups (the different primitive groups within the project)
                
primGroups = []                

for group in groups:
    primGroups.append(group)

#### setting one of the continent groups (Difining which groups are the continents that will be calculated over with pathfinding)  
    
cont1 = primGroups[0]
cont2 = primGroups[1]
cont3 = primGroups[2]
cont4 = primGroups[3]
cont5 = primGroups[4]
cont1Prims = cont1.prims()
cont2Prims = cont2.prims()
cont3Prims = cont3.prims()
cont4Prims = cont4.prims()
cont5Prims = cont5.prims()

#### getting group indexes (finding all the points that are within one of the primitive groups)

cont1Indexes = []
cont2Indexes = []
cont3Indexes = []
cont4Indexes = []
cont5Indexes = []


for c in cont1Prims:
    cont1Indexes.append(c.intrinsicValue('indexorder'))
for c in cont2Prims:
    cont2Indexes.append(c.intrinsicValue('indexorder'))  
for c in cont3Prims:
    cont3Indexes.append(c.intrinsicValue('indexorder'))
for c in cont4Prims:
    cont4Indexes.append(c.intrinsicValue('indexorder'))
for c in cont5Prims:
    cont5Indexes.append(c.intrinsicValue('indexorder'))

    
#### all networkx graph nodes (getting all the primitives within the networkx graph)
        
graphNodes = []
for nodes in graph.nodes():
    graphNodes.append(nodes)    

#### continent pathfinders (finding the paths if the conditions are met)
        
otherNodes1 = []    
pathlists1 = []
for prim in prims:
    if prim.attribValue('GDP') > 5:
        primNum = prim.intrinsicValue('indexorder')
        for nodes in graphNodes:            
            if primNum == nodes:
                sourceNode = nodes                
                for cont in cont1Indexes:
                    if sourceNode == cont:                        
                        sourceNode = sourceNode
                        otherNodes1.append(sourceNode)
                        for node in otherNodes1: 
                            if node != sourceNode:  
                                pathlists1.append(nx.dijkstra_path(graph, sourceNode, node))
otherNodes2 = []
pathlists2 = []

for prim in prims:
    if prim.attribValue('GDP') > 5:
        primNum = prim.intrinsicValue('indexorder')
        for nodes in graphNodes:
            if primNum == nodes:
                sourceNode = nodes                
                for cont in cont2Indexes:
                    if sourceNode == cont:
                        sourceNode = sourceNode
                        otherNodes2.append(sourceNode)
                        for node in otherNodes2: 
                            if node != sourceNode:  
                                pathlists2.append(nx.dijkstra_path(graph, sourceNode, node))
                                
otherNodes3 = []
pathlists3 = []
for prim in prims:
    if prim.attribValue('GDP') > 5:
        primNum = prim.intrinsicValue('indexorder')
        for nodes in graphNodes:            
            if primNum == nodes:
                sourceNode = nodes                
                for cont in cont3Indexes:
                    if sourceNode == cont:                        
                        sourceNode = sourceNode
                        otherNodes3.append(sourceNode)
                        for node in otherNodes3: 
                            if node != sourceNode:  
                                pathlists3.append(nx.dijkstra_path(graph, sourceNode, node))
                                pass

pathlistsTotal = []

pathlistsTotal.append(pathlists1)
pathlistsTotal.append(pathlists2)
pathlistsTotal.append(pathlists3)


otherNodesTotal = []

for nodes in otherNodes1:
    otherNodesTotal.append(nodes)
for nodes in otherNodes2:
    otherNodesTotal.append(nodes)
for nodes in otherNodes3:
    otherNodesTotal.append(nodes)
    

#### finding centrality (adding points for every time a primitive is passed over by a path)

centValues = dict.fromkeys(otherNodesTotal, 0)

for list in pathlistsTotal:
    for path in list:
        pathlist = path[1:]
        if len(pathlist) > 2 and len(pathlist) < 5:        
            for pathNode in pathlist:
                for prim in prims:            
                    if prim.intrinsicValue('indexorder') == pathNode:
                        for key, value in centValues.iteritems():
                            if key == pathNode:
                                centValues[key] +=1
     
for key, value in centValues.iteritems():
    for prim in prims:
        if key == prim.intrinsicValue('indexorder'):    
            prim.setAttribValue('Centrality', float(value)) 


randFloat = rd.uniform


#### GDP setter (reconfigures the gdp in a specific given ammount of timebound steps.)

prims = []

for prim in geo.prims():
    prims.append(prim)

calcFrames = 10

gdpTotal = 0.0 
centralityTotal = 0.0
for prim in geo.prims():
    centralityTotal += float(prim.attribValue('Centrality'))
    gdpTotal += prim.attribValue('GDP')

for prim in geo.prims():
    i = 1
    cPercentage = prim.attribValue('Centrality')/centralityTotal
    gdpEnd = cPercentage * gdpTotal
    gdpStart = prim.attribValue('GDP')
    gdpDifference = gdpEnd - gdpStart
    gdpPerStep = gdpDifference / calcFrames
    prim.setAttribValue('GDP', prim.attribValue('GDP') + gdpPerStep)
   
