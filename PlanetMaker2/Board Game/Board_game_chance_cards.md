# Chance Cards:
## Mobility Cards:

Donald Trump has constructed a border wall next to your region. \
**You cannot expand for the next round.** \
*Immediate Effect* 

Help Donald Trump construct a border wall around another player.\
**He cannot expand for the next round.**\
*One time use*

Elon Musk is campaigning for his Rocketship program within your region. \
**You can expand to anywhere on the earth.** \
*One time use + 6 Emissions*

New roads are built. You can travel by car now. \
**You can travel a distance of two regions.** \
*One time use + 2 Emissions*

As soon as you reached 8 population. You developed airplanes to fly over the world. \
**You can travel a distance of 10 regions.** \
*One time use + 5 Emissions*

**You built a boat to travel over the ocean.** \
*One time use + 3 Emissions*

A new hyperloop network is constructing nearby. \
**You can now expand to any connecting region.** \
_One time use + 1 Emissions_

## Agricultural Cards:

More and more inhabitants on your regions are turning vegan \
\-**1 Emission** , *Immediate Effect*

It’s not possible to distribute fresh water anymore, so lots of crops die because of dehydrated ground. \
\-**1 Agrictulture**, *Immediate Effect*

The biodiversity is going down because of emissions and low water availability \
\-**1 Population**, *Immediate Effect* 

No one is wasting food anymore \
\+**1 Agrictulture**, *Immediate Effect*


Due to trade delays, you have limited options importing food. You need to be able to provide for yourself. \
**Compare the number of population pins with the number of agricultural pins, If the number of population exceed the agricultural pins then you have to remove the excess pins.** \
*Immediate Effect*

You have the option to place tariffs on the import of food for another player. \
**Compare the number of population pins with the number of agricultural pins, If the number of population exceed the agricultural pins then he has to remove the excess pins.** \
*You can pick any player. One time use.*

Famine has struck your cities. \
**Remove one population pin from your regions** \
*Immediate Effect*




## Population Cards:

The people of your regions are migrating away. \
**Remove one of your population pins and give it to another player.** \
_One time use._

Your regions have great job opportunities. People from other regions want to migrate to your regions. \
__Remove one of a players population. Place a new population in your region.__ \
_One time use._ 


## Climate / Emission Cards:

A Tornado has stricken your regions! \
**Migrate one of your population pins to an neighbour region. If there’s no place, you have to remove the population from the board.** \
_Immediate Effect_

A Tsunami has hit! \
**Migrate one of your population pins next to the coast to a region further inland. If there’s no place, you have to remove the population from the board.** \
*Immediate Effect*

Due to droughts a forest fire occurred! \
__Remove one of your forest pins.__\
\+_2 Emission_

You have developed carbon capture \
\-_1 Emission_

You have developed to build and live on water \
\-_1 Emission_

Because of sea level rise regions become flooded.
**Roll the dice. The number says how many ocean triangles you need to place. You can choose where you want to place them, also on populated ones. If there’s a population the player has to migrate, so that costs +1 Emission, or remove it if there’s no place to migrate to. If there’s forest or agriculture the player has to remove it.**







