//substract natural landuse for agriculture
//calculating the growth number of agriculture and make it into a variable
float Grow_Agri = @New_Agriculture - @Agriculture3_P;

//Substracting the amount of new agriculture from forest and nonforest acording on the available land in the triangle
if(@New_Forest > 0 && @New_Non_Forest > 0)
   {
   @New_Forest = @New_Forest - (Grow_Agri/2);
   @New_Non_Forest = @New_Non_Forest - (Grow_Agri/2);
   }

if(@New_Forest > 0 && @New_Non_Forest == 0)
   {
   @New_Forest = @New_Forest - Grow_Agri;
   }

if(@New_Forest == 0 && @New_Non_Forest > 0)
   {
   @New_Non_Forest = @New_Non_Forest - Grow_Agri;
   }