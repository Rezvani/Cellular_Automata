//Food production from ratio of amount of food and surface of agriculture

//REGULAR
@Food_Production = (@Cereals + @Dairy + @Eggs + @Fruit + @Beef + @Pork + @Chicken + @Veg)/(@New_Agriculture);

//MEAT
//@Food_Production = (@Beef + @Pork + @Chicken)/(@New_Agriculture);

//VEGETARIAN
// @veg = @veg + @Beef;
// @veg = @veg + @Pork;
// @veg = @veg + @Chicken;
// @Food_Production = ((@Cereals + @Dairy + @Eggs + @Fruit + @Veg)/(@New_Agriculture);

//VEGAN
// @veg = @veg + @Beef;
// @veg = @veg + @Pork;
// @veg = @veg + @Chicken;
// @veg = @veg + @Dairy;
// @veg = @veg + @Eggs;
// @Food_Production = (@Cereals + @Eggs + @Fruit + @Veg)/(@New_Agriculture);

//VEGETABLES
//@Food_Production = @Veg/(@New_Agriculture);

//CEREALS
//@Food_Production = @Cereals/(@New_Agriculture);