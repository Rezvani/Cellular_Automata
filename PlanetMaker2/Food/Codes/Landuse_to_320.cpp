//This is to transfer landusedata form 20480 triangle to 320 triangles

//Making the variables needed
float Forest1_P;
float Non_Forest2_P;
float Agriculture3_P;
float Barren6_P;

float Forest_1;
float Non_Forest2;
float Agriculture3;
float Barren6;

//Making a loop that looks at every 64 triangles in every triangle from the 320 triangle model
float count = findattribvalcount(1,"prim", "origi", i@primnum);
for(int i=0; i < count; i++)
{
   float VALUE = findattribval(1,"prim", "origi", i@primnum, i);
   float LANDUSE = prim(1, "landuse_id", VALUE);
   if (LANDUSE == 1)  Forest_1 += LANDUSE;
   if (LANDUSE == 2)  Non_Forest2 += LANDUSE;
   if (LANDUSE == 3)  Agriculture3 += LANDUSE;
   if (LANDUSE == 6)  Barren6 += LANDUSE;
}

//Transfering data to percentages
Forest1_P = (Forest_1) / count;
Non_Forest2_P = (Non_Forest2) / (count*2);
Agriculture3_P = (Agriculture3) / (count*3);
Barren6_P = (Barren6) / (count*6);

//Making sure that agriculture grows when there are people living
if (Agriculture3_P == 0.0 && @count_pop > 0)
    {
    Agriculture3_P = 0.015625;
    Forest1_P =- 0.015625;
    }