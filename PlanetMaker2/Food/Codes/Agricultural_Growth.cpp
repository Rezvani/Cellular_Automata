//The growth of agriculture due to the growth of population
//Getting primitive attributes
//Agriculture can only grow in places with Arable land like forest and non forest
//Telling agriculture to grow with the percentage of the growth of population
//For Biotech we make the growth slower with a variable
float Biotech = 1.0; //if You want to make the Yield 2 times as high then it becomes 0.5 etc.
if( @New_Forest > 0 || @New_Non_Forest > 0 )
    {
    @New_Agriculture = (@New_Agriculture * (@Grow_Pop) * Biotech);
    }