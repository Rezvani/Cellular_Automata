//Calculating data from 20480 tot 320 so 64 small triangles to 1 big
//This does not work for data like landuse
//This is with a great help from Bowen

//Making a loop that looks at every 64 triangles in every triangle from the 320 triangle model
//Do this for GDP, POP and Water
float count = findattribvalcount(1,"prim", "origi", i@primnum);
for(int i=0; i < count; i++)
{
    int VALUE = findattribval(1,"prim", "origi", i@primnum, i);
    int count_pop = 0;
    int  count_gdp = 0;
    int  count_Water = 0;
    float add_pop = prim(1, "pop", VALUE); 
    float add_gdp = prim(1, "gdp", VALUE); 
    float add_Water = prim(1, "Water", VALUE); 
    @count_pop += add_pop;
    @count_gdp += add_gdp;
    @count_Water += add_Water;
}