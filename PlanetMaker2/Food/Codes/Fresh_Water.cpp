//AMOUNT OF FRESH WATER PER TYPE OF FOOD
@CerealsW = (@Cereals * 1.64);
@W_Dairy = ((@Dairy*0.0001) * 1.02);
@W_Eggs = (@Eggs * 3.27 );
@W_Fruit = (@Fruit * 0.96 );
@W_Beef = (@Beef * 15.42 );
@W_Pork = (@Pork * 5.99 );
@W_Chicken = (@Chicken * 4.33);
@W_Veg = (@Veg * 0.32);

@new_water_usage = (@W_Cereals + @W_Dairy + @W_Eggs + @W_Fruit + @W_Beef + @W_Pork + @W_Chicken + @W_Veg);

//VEGETARIAN
//@new_water_usage = ((@W_Cereals * 0.75) + @W_Dairy + @W_Eggs + @W_Fruit + (@W_Veg * 0.75));

//VEGAN
//@new_water_usage = ((@W_Cereals * 0.6) + @W_Fruit + (@W_Veg * 0.6));

//VEGETABLES
//@new_water_usage = (@W_Veg * 0.6);

//NEW AMOUNT OF FRESH WATER USAGE BASED ON GROWTH OF POPULATION
@pop_water_usage = @new_water_usage * @change_pop;

//REMAINING WATER TO BE VISUALIZED
float factor = 0.026;
@depletion = (@pop_water_usage * factor);
@remaining_water = @Water - @depletion;

//triangle with no w_usage do not have waste
if (@tot_water_usage == 0.0)    {factor = 1;}
else    {factor = 0.026;}

//extrusion never gets negative
if (@remaining_water < 0.0)
 {@remaining_water = 0.0;}

//make it red when dead
i@deadT = 0;
if (@remaining_water <= 10)    {@deadT = 0;}
else    {@deadT = 1;
}

//If the substraction is bigger than 1/128 than it should substract 1/64 times the amount from the percentage of Agriculture and should add same amount to this to forest       
int counter = 0;        
for(int i=0; i < 64; i++)
{
   if (@Agri_P < (counter/64) )
        {
        @Forest_P += (counter/64);
        @Agri_P =- (counter/64);
        counter =+ i;
        }
    }
