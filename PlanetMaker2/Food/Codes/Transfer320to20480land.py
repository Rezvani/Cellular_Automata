#This is a script where Bowen helpt us a lot

#Importing the libraries
node = hou.pwd()
geo = node.geometry()
import math
import random
import numpy as np

#Making emty arrays
listFtoA = []
listNFtoA = []

#get the number of triangles to change
for prim in geo.prims():

    tri_id = prim.attribValue('tri_id')
    landuse_id = prim.attribValue('landuse_id')
    P_FtoA = prim.attribValue('changeFtoA_PN')
    P_NFtoA = prim.attribValue('changeNFtoA_PN')
    if landuse_id == 1:
        listFtoA.append(tri_id)
    if landuse_id == 2:
        listNFtoA.append(tri_id)

trinum_FtoA = float(P_FtoA)*len(listFtoA)
trinum_NFtoA = float(P_NFtoA)*len(listNFtoA)
list_changeFtoA = []
list_changeNFtoA = []
if trinum_FtoA >0:
    list_changeFtoA = listFtoA[0:int(round(trinum_FtoA))]    
if trinum_NFtoA >0:
    list_changeNFtoA = listNFtoA[0:int(round(trinum_NFtoA))]

#change the triangle in the random list    
for prim in geo.prims():
    tri_id = prim.attribValue('tri_id')
    if tri_id in list_changeFtoA:
        prim.setAttribValue('landuse_id', 3)
        prim.setAttribValue('change', 1)
    if tri_id in list_changeNFtoA:
        prim.setAttribValue('landuse_id', 3)
        prim.setAttribValue('change', 1)