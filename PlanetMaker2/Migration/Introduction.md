# Introduction Migration Folder
This is the Folder of the Migration group simulation engine. 

## Contributors

Minnie Lok Yan Chu\
Tiger Ziou Gao\
Huudat Nguyen

## Contents

In this folder you may find:
1. The python script for the gravity model (made by Huudat Nguyen)
2. Databases: raw and processed + jupyter notebook scritps (made by Minnie Chu)
3. The final houdini file of Huudat Nguyen (simulation script)
4. The final houdini file of Ziou Gao (Tiger) (globe model and dymaxion map with data projections) 
5. some screenshots (see below)
6. some small python scripts (made by Ziou Gao)


![](photos/attribpromote.png)
![](photos/dym5.png)
![](photos/Extrusions.png)
