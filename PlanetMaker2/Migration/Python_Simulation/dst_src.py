"""
test to get all destination and source points
"""
dst = []
src = []

"""
Defining Weights

Made by Tiger (Ziou)
"""

import numpy as np
from scipy import inf
import csv
from csv import reader

f_1 = open(r"/Users/ziougao/repositories/planet-maker-2.0/Shared_databases_python_script/adjencies320.csv") #import neighborlist
adjencies = reader(f_1)
list_adjencies = list(adjencies)
array_adjencies = np.array(list_adjencies, dtype=int)
#print(array_adjencies)

f_3 = open("/Users/ziougao/Desktop/land_water.csv") #import weight (sea)
testvals = reader(f_3)
list_testvals = list(testvals)
array_testvals = np.array(list_testvals, dtype=int)
array_vals = array_testvals.ravel() #flattened 1D array of 320 nodes: 1 is land, 0 is sea

f_4 = open("/Users/ziougao/Desktop/elev_score1.csv") #import weight (altitude)
testvals1 = reader(f_4)
list_testvals1 = list(testvals1)
array_testvals1 = np.array(list_testvals1, dtype=int)
array_vals1 = array_testvals1.ravel() #flattened 1D array of 320 nodes: 1 is land, 0 is sea
print(array_vals1)

"""
Test:
weights are given by altitude:
0-300       1
300-600     2
600-900     3
900-1200    4
1200-1500   5
1500-2000   6
2000-2500   7
2500-3000   8
>3000       9
"""

matrix = np.zeros((320,320)) #zero matrix 320x320

for i in range(len(matrix)): #make path with neighbors
    neighs = array_adjencies[i]
    for neigh in neighs: 
        matrix[i][neigh] = 1 #give all neighbours weight 1

    for j in range(len(matrix)):
        if matrix[i][j] != matrix[i][neigh]:
            matrix[i][j] = inf #make everything else inf
        if matrix[i][j] == matrix[i][i]:
            matrix[i][j] = 0 #make matrix[i][i] = 0

print(matrix)

for row in range(len(matrix)): #iterating over the rows
    for index in range(len(array_vals)): #iterating over triangle_number = index (columns)
        land = array_vals[index] 
        if land == 0: #search in all sea triangles
            if matrix[row][index] == 1: #search in all neighbour triangles
                matrix[row][index] = 20 #give weight 20
    
        elevation = array_vals1[index]
        if matrix[row][index] == 1: 
            matrix[row][index] = elevation #give altitude score

"""
matrix now is just a random one (the weights matrix)
Once in houdini, it should be the Tij matrix from the gravity model
"""

for row in range(len(matrix)):
    for index in range(len(matrix)):
        if matrix[row][index] > 1:
            dst.append(row)
            src.append(index)

print(dst)
print(src)