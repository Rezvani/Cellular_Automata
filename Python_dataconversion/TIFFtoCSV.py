# -*- coding: utf-8 -*-
"""
Created on Wed May 30 14:23:55 2018

@author: Puck
"""

from osgeo import gdal,ogr
from osgeo.gdalconst import *
import struct
import sys
import os
import gdal
import numpy
import csv

#following function will convert the geotiff population data to a readable value
def pt2fmt(pt):
	fmttypes = {
		GDT_Byte: 'B',
		GDT_Int16: 'h',
		GDT_UInt16: 'H',
		GDT_Int32: 'i',
		GDT_UInt32: 'I',
		GDT_Float32: 'f',
		GDT_Float64: 'f'
		}
	return fmttypes.get(pt, 'x')
#input tif file
tif_file = "C:\Users\Puck\Documents\Geomatics\AR0078\population_small.tif"
ds = gdal.Open(tif_file)

#parameters for later input
transf = ds.GetGeoTransform()
band = ds.GetRasterBand(1)
bandtype = gdal.GetDataTypeName(band.DataType) #Int16
transfInv = gdal.InvGeoTransform(transf)

#to count the number of existing and non-existing lat/lon values
error = 0
correct = 0

#define output file to write your data to. 
outfile = "population_CSV.csv"
outfile_open = open(outfile,'w')
#define header of file
outfile_open.write("lat, lon, population \n")

# Loop through all lon/lat values per 1 degree. If you have more detailed data, say at every 0.1 degree, find a way how
# to loop through non-integer numbers
for lat in range(-90,+90,1): #range(min,max,stepsize)
    for lon in range(-180,+180,1):
        # sometimes a tif file has no data for some lat,lon combinations. I write 0 in this case, but you can
        # also remove the statement at except.
        try:
            #find the x and y value of the pixel according to lat,lon
            px, py = gdal.ApplyGeoTransform(transfInv, lon, lat)
            
            #find value belonging to this x and y
            structval = band.ReadRaster(int(px), int(py), 1,1, buf_type = band.DataType )
            fmt = pt2fmt(band.DataType)
            
            intval = struct.unpack(fmt , structval)
            popval= round(intval[0],2)
            
            #the following is specific to population data. Some pixels had a very large negative number, which are assumed as population = 0
            if popval < 0:
                outfile_open.write(str(lat)+','+str(lon)+','+str(0.0)+ "\n")
            else:
                outfile_open.write(str(lat)+','+str(lon)+','+str(popval)+ "\n")
            
            correct +=1
        except:
            outfile_open.write(str(lat)+','+str(lon)+','+str(0.0)+ "\n")
            error +=1
print correct, error
outfile_open.close()