"""
BZZZ ZZZy  ZZZZZZZ  ,ZZZZZZZ  ZZZB ZZZ    ZZZZZz    ZZZ,ZZZj  ZZZZ ZZZ       ZZZ BZZZ  DZZZ,  ZZZZ  ZZZZ
 ZZ   Z5    Zj w 5   wZ   j,   zZ  ZW    ZZ   yZE   DZw  Z9    Z Zw ZW       Zy ZZ     9Z     Z,Zj Z Z
 ZZZZZZz    ZZZZ     jZZZz      8ZZD     ZZ    ZZ   zZj  ZZ    Z 8Z Zy       ZZZZ      ZZ     Z EZ Z Z
 ZZ   ZD    Zy  DZ   WZ   ZB     ZZ      ZZj  ZZD   8ZD  ZZ    Z  Z8Z5       ZD 8Zy    EZ     Z  ZZj Z
8ZZZ ZZZD  ZZZZZZZ  WZZZZZZ9    ZZZZ      jZZZZ      8ZZZZ    ZZZ wZZz       ZZZy9ZZZ  zZZZw  ZZZ ZZ ZZZ

20180627 @Heeyoun Kim
2018 Why Factory_Future Model_Team Energy

# 20180516 - game engine ver 0.2   : START to organize as class
# 20180516 - game engine ver 0.4   : energy satisfaction done / distribution _ dummy chacked
# 20180516 - game engine ver 0.45  : adapt updated flow chart
# 20180516 - game engine ver 0.5   : definitions for non_selected triangles is prepared
# 20180516 - game engine ver 0.55  : Psychological distance / EU emigration model / print codes are turned off.
# 20180627 - game engine ver 0.6   : eliminated useless, previous codee ----! UPDATE GITHUB
"""
class calc_energygame:

    ###################################
    ### I N I T I A L I Z A T I O N ###
    ###################################

    def __init__(self):
        self.value1 = 0
        self.value2 = 0
        self.value3 = 0
        #print ('game engine is ready')

    #########################################
    ### P O P U L A T I O N   G R O W T H ###
    #########################################
    def population_calc(self, value1, value2, value3):

        prev_pop = value1
        turn_number = value2
        prev_pol = value3

        result = prev_pop * (1 + (0.01 * ((-0.015 * turn_number + 0.95) - (0.002 * prev_pol))))

        #print ("CURRENT POPULATION: ", result)

        return result

    #####################################################
    ### E N E R G Y   N E E D S   P E R   C A P I T A ###
    #####################################################
    def en_calc(self, value1):
        temp = value1
        #architype = value2
        if temp <= 10:
            result = 24685

        elif temp > 10 and temp < 25:
            result = 36400

        elif temp >= 25:
            result = 66107

        #print ("ENERGY NEEDS PER CAPITA:  : ", result)

        return result

    #############################################
    ### D E M A N D S   C A L C U L A T O R S ###
    #############################################

    ### ENERGY DEMANDS
    def ed_calc(self, value1, value2):

        energy_needs = value1
        calculated_pop = value2

        result = ((energy_needs * 365) * (10 ** (-9)) * calculated_pop)
        #print ("ENERGY DEMANDS " , result)

        return result

    ### Renewable demands
    def rd_calc(self, value1, value2):
        energy_demands = value1
        transition_value = value2

        result = energy_demands * transition_value
        #print ("RENEWABLE ENERGY DEMANDS: ", result)

        return result

    ### Fossile Demands
    def fd_calc(self, value1, value2):

        energy_demands = value1
        renewable_demands = value2

        result = energy_demands - renewable_demands
        #print ("FOSSILE FULE DEMANDS",  result)

        return result

    #####################################################
    ### F O S S I L E   F U E L S   P R O D U C T O R ###
    #####################################################

    ### O I L ###
    def oil_prod(self, value1, value2, value3):
        prev_oil_reserve = value1
        fossil_demand = value2
        user_input = float(value3)

        if prev_oil_reserve == 0 or prev_oil_reserve < 1:
            result = 0
        else:
            if user_input == 0:
                result = fossil_demand * 0.44
            else:
                result = fossil_demand * (user_input / 100)
                #print("fossile_demand is", fossil_demand)
                #print("user input is ", user_input)

        #print ('OIL PRODUCTION:  ', result)
        return result

    def non_tri_oil_prod(self, value1, value2):
        prev_oil_reserve = value1
        fossil_demand = value2

        if prev_oil_reserve == 0 or prev_oil_reserve < 1:
            result = 0
        else:
            result = fossil_demand * 0.44

        #print ('nt_OIL PRODUCTION:  ', result)
        return float(result)

    def oil_reserve(self, value1, value2):
        perv_oil_reserve = value1
        oil_prod = value2
        result = perv_oil_reserve - oil_prod
        #print('OIL RESERVE: ', result)
        return result


    ### C O A L ###
    def coal_prod(self, value1, value2, value3):
        prev_coal_reserve = value1
        fossil_demand = value2
        user_input = float(value3)

        if prev_coal_reserve == 0 or prev_coal_reserve < 1:
            result = 0
        else:
            if user_input == 0:
                result = fossil_demand * 0.44
            else:
                result = fossil_demand * (user_input / 100)
                #print("fossile_demand is", fossil_demand)
                #print("user input is ", user_input)

        #print ('COAL PRODUCTION: ', result)
        return result

    def non_tri_coal_prod(self, value1, value2):
        prev_coal_reserve = value1
        fossil_demand = value2

        if prev_coal_reserve == 0 or prev_coal_reserve < 1:
            result = 0
        else:
            result = fossil_demand * 0.44

        #print ('nt_COAL PRODUCTION:  ', result)
        return result

    def coal_reserve(self, value1, value2):
        perv_coal_reserve = value1
        coal_prod = value2
        result = perv_coal_reserve - coal_prod
        #print('COAL PRODUCTION :', result)

        return result

    ### G A S ###
    def gas_prod(self, value1, value2, value3):
        prev_gas_reserve = value1
        fossil_demand = value2
        user_input = float(value3)

        if prev_gas_reserve == 0 or prev_gas_reserve < 1:
            result = 0
        else:
            if user_input == 0:
                result = fossil_demand * 0.44
            else:
                result = fossil_demand * (user_input / 100)
                #print("fossile_demand is", fossil_demand)
                #print("user input is ", user_input)

        #print ('GAS PRODUCTION: ', result)
        return result

    def non_tri_gas_prod(self, value1, value2):
        prev_gas_reserve = value1
        fossil_demand = value2

        if prev_gas_reserve == 0 or prev_gas_reserve < 1:
            result = 0
        else:
            result = fossil_demand * 0.44

        #print ('nt_GAS PRODUCTION:  ', result)
        return result

    def gas_reserve(self, value1, value2):
        perv_gas_reserve = value1
        gas_prod = value2
        result = perv_gas_reserve - gas_prod
        #print('GAS RESERVES: ', result)

        return result

    ###########################################################
    ### R E N E W A B L E   E N E R G Y   P R O D U C T O R ###
    ###########################################################

    def renew_solar_prod(self, value1, value2, value3):
        solar_poten = value1
        total_poten = value2
        renew_demands = value3

        solar_prod = (solar_poten / total_poten) * renew_demands
        #print ('SOLAR PRODUCTION', solar_prod)

        return solar_prod

    def renew_rain_prod(self, value1, value2, value3):
        rain_poten = value1
        total_poten = value2
        renew_demands = value3

        rain_prod = (rain_poten / total_poten) * renew_demands
        #print ('RAIN PRODUCTION', rain_prod)

        return rain_prod

    def renew_wind_prod(self, value1, value2, value3):
        wind_poten = value1
        total_poten = value2
        renew_demands = value3

        wind_prod = (wind_poten / total_poten) * renew_demands
        #print ('WIND PRODUCTION', wind_prod)

        return wind_prod

    def renew_water_prod(self, value1, value2, value3):
        water_poten = value1
        total_poten = value2
        renew_demands = value3

        water_prod = (water_poten / total_poten) * renew_demands
        #print ('SOLAR PRODUCTION', water_prod)

        return water_prod

    def renew_total_prod(self, value1, value2, value3, value4):
        solar = value1
        rain = value2
        wind = value3
        water = value4

        renew_total_prod = solar + rain + wind + water
        #print("TOTAL RENEWABLE ENERGY PRODUCTION :", renew_total_prod)
        return renew_total_prod

    ###########################################
    ### O V E R A L L   P R O D U C T I O N ###
    ###########################################

    def fossil_prod(self, value1, value2, value3):
        oil_prod = value1
        coal_prod = value2
        gas_prod = value3

        result = oil_prod + coal_prod + gas_prod

        #print ("TOTAL FOSSIL ENERGY PRODUCTION: ", result)

        return result

    def overall_production(self, value1, value2):
        fossil_prod = value1
        renew_prod = value2

        result = fossil_prod + renew_prod

        #print ("TOTAL ENEFGY PRODUCTION: ", result)

        return result

    #################
    ### D E L T A ###
    #################
    def delta_calc(self, value1, value2):
        energy_demands = value1
        overall_prod = value2
        result = overall_prod - energy_demands

        #print ("DELTA :", result)

        return result

    def delta_checker (self, value1, value2):
        delta_a = value1
        delta_b = value2
        bigger_tri= ""
        smaller_tri = ""

        if delta_a > delta_b:
            if self.neg_checker(delta_b) == True:
                result = delta_a
            else:
                result = delta_a - delta_b
            bigger_tri = 'triangle A'
            smaller_tri = 'triangle B'
            #print("#### D E L T A   C H E C K E R ####")
            #print(bigger_tri, "will send ", result, " energy to ", smaller_tri)

        elif delta_b > delta_a:
            if self.neg_checker(delta_a) == True:
                result = delta_b
            else:
                result = delta_b - delta_a
            bigger_tri = 'triangle B'
            smaller_tri = 'triangle A'
            #print("#### D E L T A   C H E C K E R ####")
            #print(bigger_tri, "will send ", result, " energy to ", smaller_tri)

        elif delta_a == delta_b:
            result = 0
            #print("#### D E L T A   C H E C K E R ####")
            #print ('triangle A  and triangle B have same delta')

        return result


    ##########################
    ###  P O L U T I O N   ###
    ##########################
    def pol_calc(self, value1):
        fossil_prod = value1
        result = fossil_prod * 6.56213 * (10 ** -5)

        #print ("POLLUTION : " , result)

        return result


    ###########################################
    ### E N E R G Y   S U P P L Y   R A T E ###
    ###########################################
    def energy_supply(self, value1, value2, value3, value4, value5):
        dist_value = float(value1)
        prod_value = float(value2)
        energy_demands = float(value3)

        delta_x = value4
        delta_total = value5

        result = 0

        if delta_x < delta_total :
            result = ((dist_value + prod_value) / energy_demands) * 100
            #print ("ENERGY SUPPLY/ SOURCE : ", result, "percent")


        elif delta_x > delta_total:
            result = ((-(dist_value) + prod_value) / energy_demands) * 100
            #print ("ENERGY SUPPLY/ TARGET :", result, "percent")

        return result

    def non_tri_energy_supply(self, value1, value2):
        delta_value= value1
        energy_demands = value2

        if energy_demands == 0 :
            result =  0

        else:
            result = (delta_value / energy_demands) * 100

        #print ('non_selected_triangle energy supply', result)

        return result


    ############################################
    ### T E M P E R A T U R E   C H A N G E  ###
    ############################################
    def temp_change(self, value1):
        pollution = value1

        result = (pollution * (10**-6)) *640
        #print ('TEMPERATURE CHANGE :', result)
        return result


    def temperature(self, value1, value2):
        prev_temp = value1
        temp_change = value2

        result = prev_temp + temp_change
        #print ('TEMPERATURE_NEXTTURN: ', result)

        return result

    def temp_delta(self, value1, value2):
        prev_temp_delta = value1
        temp_change = value2

        result = prev_temp_delta + temp_change
        #print('TEMPERATURE DELTA :', result)

        return result


    #######################################################
    ##### P S Y C H O L O G I C A L   D I S T A N C E #####
    #######################################################

    def psy_distance(self, value1):
        temp_delta = value1
        result = temp_delta/ 5
        #print("\n\n##############################")
        #print("PSYCHOLOGICAL DISTANCE: ", result)
        #print("##############################\n\n")


        return result

    #####################################################
    ##### P O P U L A T I O N   E M I G R A T I N G #####
    #####################################################

    def population_emigrating(self, value1, value2, value3, value4):
        en_supply_a = value1
        en_supply_b = value2
        pollution = float(value3)
        population = float(value4)
        criteria_es = max([en_supply_a, en_supply_b])
        result = ((((-1 * criteria_es) + 100) + (pollution / 1000)) / 1000) * population
        #print("\n\n##############################")
        #print ("EMIGRATING : " , result)
        #print("##############################\n\n")
        return result

    '''
    def population_change(self, value1, value2):
        population = value1
        popu_emig = value2

        result = (population + popu_emig)
        #print("\n\n##############################")
        #print ("POPULATION CHANGE: ", result)
        #print("##############################\n\n")

        return result
    '''

    def population_change(self, value1, value2, value3, value4, value5):
        triangle_name = value1
        en_supply_a = float(value2)
        en_supply_b = float(value3)
        popu_triangle = value4
        popu_emig = value5

        if self.tri_checker(triangle_name) == 0:
            if en_supply_a > en_supply_b:
                result = popu_triangle + popu_emig
            elif en_supply_a < en_supply_b:
                result = popu_triangle - popu_emig


        elif self.tri_checker(triangle_name) == 1:
            if en_supply_b > en_supply_a:
                result = popu_triangle + popu_emig
            if en_supply_b < en_supply_a:
                result = popu_triangle - popu_emig

        if result <= 0:
            result  = 0

        return result

    def city_empty(self, value1, value2, value3):
        popu_changed = float(value1)
        popu_emig = float(value2)
        popu_default = float(value3)

        result = 1 - ((popu_changed - popu_emig) / popu_default)
        #print("\n\n##############################")
        #print ("CITY EMPTY: ", result)
        #print("##############################\n\n")

        return result

    ########################################
    ### T R A N S I T I O N   V A L U E  ###
    ########################################
    def trans_value(self, value1, value2, value3):
        prev_trans_value = float(value1)
        psy_dist = float(value2)
        city_empty = float(value3)

        result = prev_trans_value + ((psy_dist * city_empty)/10)
        #print("\n\n##############################")
        print ("TRANSITION VALUE : ", result)
        #print("##############################\n\n")
        return result


    #######################
    ### C H E C K E R S ###
    #######################
    def neg_checker(self, value1):
        get_number = str(value1)

        if get_number == '':
            return False
        elif get_number[0] == '-':
            #print (get_number, get_number[0], 'see? this is nega!')
            return True
        else:
            return False

    def tri_checker(self, value1):
        triangle_name = value1

        if triangle_name[-1] == 'a':
            result = 0
        if triangle_name[-1] == 'b':
            result = 1
        return result









