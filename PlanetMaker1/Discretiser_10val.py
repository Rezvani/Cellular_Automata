# -*- coding: utf-8 -*-
"""
Created on Wed Jun 06 14:17:14 2018

@author: Hellmer
"""

import pandas as pd
import numpy as np

def df_discretiser_10(df, dfcomp): # discretises dataframe into highs and lows depending on their average
    df2=pd.DataFrame() 
    def col_average (datafrm, col_name): #creates average from a column
        col_value=datafrm[[col_name]]
        nums=col_value.loc[1:len(col_value)]
        y=np.mean(nums)
        x=float(y)
        return x  
    collist=list(df.columns)
    for i in collist:
        bins = [dfcomp[i].min(),0.1*dfcomp[i].max(),0.2*dfcomp[i].max(),0.3*dfcomp[i].max(),0.4*dfcomp[i].max(),0.5*dfcomp[i].max(),0.6*dfcomp[i].max(),0.7*dfcomp[i].max(),0.8*dfcomp[i].max(),0.9*dfcomp[i].max(),dfcomp[i].max()]
        names = [0,1,2,3,4,5,6,7,8,9]
        df2[i]=pd.cut(df[i], bins, labels = names)
    df2.fillna(0, inplace=True)
    return df2
